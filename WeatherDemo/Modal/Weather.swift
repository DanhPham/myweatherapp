//
//  Weather.swift
//  WeatherDemo
//
//  Created by apple on 9/13/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

struct Weather {
    var id : String
    var Date : String
    var Photo : String
    var TempMin : String
    var TempMax : String
    var Description : String
    var Windspeed: String
    
    init(id:String,Date:String,Photo:String,TempMin:String,TempMax:String,Description:String,Windspeed:String){
        self.id = id
        self.Date = Date
        self.Photo = Photo
        self.TempMin = TempMin
        self.TempMax = TempMax
        self.Description = Description
        self.Windspeed = Windspeed
    }
}
