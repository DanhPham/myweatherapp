//
//  MyCustomCell.swift
//  WeatherDemo
//
//  Created by apple on 9/13/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class MyCustomCell : UITableViewCell {
    
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTempMin: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTempMax: UILabel!
    @IBOutlet weak var lblwindspeed: UILabel!
    
    public func loadData(weather : Weather) {
        lblDate.text = weather.Date
        lblID.text = weather.id
        lblTempMin.text = weather.TempMin
        lblTempMax.text = weather.TempMax
        lblDescription.text = weather.Description
        lblwindspeed.text = weather.Windspeed
    }
    
}
