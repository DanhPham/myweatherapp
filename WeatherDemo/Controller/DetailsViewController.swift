//
//  DetailsViewController.swift
//  WeatherDemo
//
//  Created by apple on 9/13/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class DetailsViewController : UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var lbDatetime: UILabel!
    @IBOutlet weak var tvTempMin: UITextView!
    @IBOutlet weak var tvTempMax: UITextView!
    @IBOutlet weak var tvWindspeed: UITextView!
    @IBOutlet weak var tvDescription: UITextView!
    
    var objWeather : Weather!
    
    @IBAction func BackClicked(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbDatetime.text = objWeather.Date
        tvTempMin.text = objWeather.TempMin
        tvTempMax.text = objWeather.TempMax
        tvWindspeed.text = objWeather.Windspeed
        tvDescription.text = objWeather.Description
        detailImage.image = UIImage(named: objWeather.Photo)
    }
}
