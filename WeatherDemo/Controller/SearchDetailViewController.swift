//
//  SearchDetailViewController.swift
//  WeatherDemo
//
//  Created by apple on 9/14/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SearchDetailViewController : UIViewController {
    
    
    //MARK: Outlet
    
    var obj2 : [Weather]!
    let datepicker = UIDatePicker()
    var isFirstLoad: Bool = false
    var searchedWeather: Weather?
    @IBOutlet weak var txtSearch: UITextField!
    
    //MARK: Action
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Function
    @objc func HandleDateChange(datepicker: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        txtSearch.text = formatter.string(from: datepicker.date)
        self.inputView?.endEditing(true)
    }
    
    @IBAction func BackClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SearchWeatherClicked(_ sender: Any) {
        for currentWeather in obj2 {
            if txtSearch.text == currentWeather.Date {
                self.searchedWeather = currentWeather
            }
        }
        self.isFirstLoad = false
            self.performSegue(withIdentifier: "segueSearchDetail", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueSearchDetail" {
            let destination = segue.destination as? DetailsViewController
            destination?.objWeather = self.searchedWeather
//            self.performSegue(withIdentifier: "segueSearchDetail", sender: self)
        }
    }
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datepicker.datePickerMode = .date
        txtSearch.inputView = datepicker
        datepicker.addTarget(self, action: #selector(HandleDateChange(datepicker:)), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !self.isFirstLoad {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
