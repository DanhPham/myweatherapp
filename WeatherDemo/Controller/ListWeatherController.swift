//
//  ListWeatherController.swift
//  WeatherDemo
//
//  Created by apple on 9/13/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ListWeatherController :  UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: Properties
    let datepicker = UIDatePicker()
    var listWeather : [Weather] = [Weather(id: "30.01.2017", Date: "30.01.2017", Photo: "icons8-sun-48.png", TempMin: "261.410000", TempMax: "262.650000", Description: "sky is clear", Windspeed: "4.57"),
                                   Weather(id: "31.01.2017", Date: "31.01.2017", Photo: "icons8-snow-48.png", TempMin: "260.980000", TempMax: "265.440000", Description: "light snow", Windspeed: "4.1"),
                                   Weather(id: "01.02.2017", Date: "01.02.2017", Photo: "icons8-snow-48.png", TempMin: "266.900000", TempMax: "270.590000", Description: "light snow", Windspeed: "4.53"),
                                   Weather(id: "02.02.2017", Date: "02.02.2017", Photo: "icons8-sun-48.png", TempMin: "255.190000", TempMax: "264.020000", Description: "sky is clear", Windspeed: "3.06"),
                                   Weather(id: "03.02.2017", Date: "03.02.2017", Photo: "icons8-snow-48.png", TempMin: "256.550000", TempMax: "266.000000", Description: "light snow", Windspeed: "7.35"),
                                   Weather(id: "04.02.2017", Date: "04.02.2017", Photo: "icons8-sun-48.png", TempMin: "254.730000", TempMax: "259.950000", Description: "sky is clear", Windspeed: "2.6")]
    
    //MARK: Outlets
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var tblListWeather: UITableView!
    
    //MARK: Function
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "segueListWeatherDetail", sender: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listWeather.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as! MyCustomCell
        let currentWeather = listWeather[indexPath.row]
        cell .loadData(weather: currentWeather)
        
        let photoLink = currentWeather.Photo
        cell.imgPhoto.image = UIImage(imageLiteralResourceName: photoLink)
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueListWeatherDetail"{
            
            guard let selectIndex = sender as? Int else {
                return
            }
            let objWeather  = listWeather[selectIndex]
            let destination = segue.destination as? DetailsViewController
            destination?.objWeather = objWeather
        
        }
        if segue.identifier == "segueListWeatherSearch" {
//            guard let selectIndex = sender as? Int else {
//                return
//            }
//            let objWeather  = listWeather[selectIndex]
            let destination = segue.destination as? SearchDetailViewController
            destination?.isFirstLoad = true
            destination?.obj2 = listWeather
        }
        
    }
    @IBAction func SearchWeatherClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "segueListWeatherSearch", sender: self)
        
    }
    
    
    
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        }
    
    
}


